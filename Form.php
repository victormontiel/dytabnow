<?php
session_start();
include('functions_EM_2.php');
$timeout=sessionTimeout();
if(!empty($timeout)) {
        redirectToLogInError("TIMEOUT");
    }

if(isset($_SESSION["Rol"]))
    {
       $Rol=$_SESSION["Rol"];
    }
else {
	redirectToLogIn("CREDENTIALS");
	}


if(isset($_GET["ViewName"]))
    {
        $ViewName = $_GET["ViewName"];
    }

//action = 0 --> insert_nothing --- action = 1 --> update_nothing
 $action=0;
 $Id=-1;
 $IdOportunidad="-1";
 $cif="-1";

 if(isset($_GET["Id"]))
    {
        $Id = $_GET["Id"];
        $action=1;
    }

    if(isset($_GET["IdOportunidad"]))
    {
        $IdOportunidad = $_GET["IdOportunidad"];
	}
	

?>

<html lang="es">

 	<head>
 		<title>BilliB ARista</title>
  		<meta charset="UTF-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
		<!-- GOOGLE FONTS + ICONS -->
    	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="style/Nuevoregistro_Form.css"/>
	</head>
	<style>
		.column {
    		float: left;
    		width: 50%;
    		padding: 10px;
		}

/* Clear floats after the columns */
		.row:after {
    		content: "";
    		display: table;
    		clear: both;
		}
		.preloader-background {
    z-index: 999;
    background: #222f3db8;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
}

.preloader-wrapper.big.active {
    position: absolute;
    top: 40%;
    left: 50%;
}

h3.preloader-msg {
    position: absolute;
    color: white;
    font-weight: 300;
    top: 50%;
    left: 40%;
}

.active {
    z-index: 999;
}

.spinner-blue-only {
    border-color: #00ffd1;
}
	</style>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js"></script>

	<script type="text/javascript">
		function submitForm(table) 
		{
			document.getElementById(table).submit();
		}

		function clearForm(Form) {
			alert('hola');
			//document.getElementById(Form).reset();
			var form = document.getElementById(Form);
			form.reset();
			var frm_elements=form.elements;
			for(var i=0;i<frm.elements.length;i++) {
				if(frm.elements[i].type=="text") {
					var field = Document.getElementById(frm.elements.id).value="";
				}
			}
			alert('x.id');
		}

		

		function goBack() {
			window.history.back();
		}

		function digitsOnly(obj) {
   obj.value = obj.value.replace(/[^0-9.,]+/g, "");
}

		function checkMandatorySelects(formDiv) {
			var y = document.getElementById(formDiv.id);
			var x = y.querySelectorAll('[name="requiredSelectDiv"]');
			for(var i=0;i<x.length;i++) {
				var labels = x[i].getElementsByTagName("LABEL");
				var selects = x[i].getElementsByTagName("SELECT");
				if(selects[0].value=="") {
					alert("El campo " + labels[0].innerText.replace("*","") + " es obligatorio.");
					return false;
				}
			}
		}

		</script>
	<body>
		<?php
		createFormHeader($ViewName, $action, $Rol);
		?>
		<div id="Preloader">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
        </div>
			<div class="Form" style="width:1200px; margin:0% auto;" id="content" >
			</div>
		<footer id="BillibFooter">
			<p style="position: relative; left: 39vw;  bottom: 13px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
			<div class="container">
				<center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
			</div>
		</footer>
        <script>
		let viewName = "<?php echo $_GET["ViewName"]; ?>";
		let action = "<?php echo $action; ?>"
		let id = "<?php echo $Id; ?>"
		let rol = "<?php echo $Rol; ?>"
		let url_reqs = "functions_route.php";
            function createTabsAndForms(){
				$.ajax({
					type: "GET",
					url: url_reqs,
					data: {"create_tabs_forms": true, "ViewName": viewName, "action":action, "id": id, "Rol": rol },
					dataType: "html",
					success: function (response) {
						$("#content").html(response);
						$("#Preloader").hide();
						$("#content").show();
						$('.datepicker').pickadate(
						{ 
						selectMonths: true,
						selectYears: 15, 
						today: 'Today',
						format: 'yyyy-mm-dd', 
						clear: 'Clear',
						close: 'Ok',
						closeOnSelect: false,
						formatSubmit: 'yyyy-mm-dd'
						});
					}
				});
			}
			$(document).ready(function () {
				$("#content").hide();
				createTabsAndForms();
				
			});

        </script>
	</body>
</html>

